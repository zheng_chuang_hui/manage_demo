import request from '@/utils/request'

// 分页查询
export function porcInstPage(data) {
    return request({
        url: '/bpm/procinst/page-todo',
        method: 'post',
        data: data
    })
}