import request from '@/utils/request'

// 创建租户信息
export function createTenantInfo(data) {
    return request({
        url: '/system/tenantInfo/create',
        method: 'post',
        data: data
    })
}

// 主键获取详情
export function getTenantInfo(id) {
    return request({
        url: '/system/tenantInfo/get/' + id,
        method: 'get'
    })
}

// 分页查询租户
export function getTenantInfoPage(data) {
    return request({
        url: '/system/tenantInfo/page',
        method: 'post',
        data: data
    })
}

// 获取所有租户类型列表
export function getAllTenantInfo() {
    return request({
        url: '/system/tenantInfo/list-all',
        method: 'get'
    })
}

// 修改租户信息
export function updateTenantInfo(data) {
    return request({
        url: '/system/tenantInfo/update',
        method: 'post',
        data: data
    })
}



// 创建租户短信
export function createTenantSMS(data) {
    return request({
        url: '/system/tenantSms/create',
        method: 'post',
        data: data
    })
}

// 删除租户短信
export function delTenantSMS(id) {
    return request({
        url: '/system/tenantSms/delete/' + id,
        method: 'get'
    })
}

// 主键获取短信
export function getTenantSMS(id) {
    return request({
        url: '/system/tenantSms/get/' + id,
        method: 'get'
    })
}

// 分页查询短信
export function getTenantSMSPage(data) {
    return request({
        url: '/system/tenantSms/page',
        method: 'post',
        data: data
    })
}

// 修改租户短信
export function updateTenantSMS(data) {
    return request({
        url: '/system/tenantSms/update',
        method: 'post',
        data: data
    })
}
