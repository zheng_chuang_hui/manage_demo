import request from '@/utils/request'

// 查询定时任务调度列表
export function listJob(query) {
  return request({
    url: '/system/quartz/job/page',
    method: 'get',
    params: query
  })
}

// 查询定时任务调度详细
export function getJob(jobId) {
  return request({
    url: '/system/quartz/job/get?id=' + jobId,
    method: 'get'
  })
}

// 新增定时任务调度
export function addJob(data) {
  return request({
    url: '/system/quartz/job/create',
    method: 'post',
    data: data
  })
}

// 修改定时任务调度
export function updateJob(data) {
  return request({
    url: '/system/quartz/job/update',
    method: 'post',
    data: data
  })
}

// 删除定时任务调度
export function delJob(jobId) {
  return request({
    url: '/system/quartz/job/delete?id=' + jobId,
    method: 'get'
  })
}

// 导出定时任务调度
export function exportJob(query) {
  return request({
    url: '/system/quartz/job/export-excel',
    method: 'get',
    params: query,
    responseType: 'blob'
  })
}

// 任务状态修改
export function updateJobStatus(jobId, status) {
  return request({
    url: '/system/quartz/job/update-status?id=' + jobId + "&status=" + status,
    method: 'get'
  })
}


// 定时任务立即执行一次
export function runJob(jobId) {
  return request({
    url: '/system/quartz/job/trigger?id=' + jobId,
    method: 'get'
  })
}

// 获得定时任务的下 n 次执行时间
export function getJobNextTimes(jobId) {
  return request({
    url: '/system/quartz/job/get_next_times?id=' + jobId,
    method: 'get'
  })
}
