import request from '@/utils/request'

// 查询缓存详细
export function getCache() {
  return request({
    url: '/system/infra/redis/get-monitor-info',
    method: 'get'
  })
}

// TODO
export function getKeyList() {
  return request({
    url: '/system/infra/redis/get-key-list',
    method: 'get'
  })
}
