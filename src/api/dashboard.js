import request from '@/utils/request'

// 查询设备预警
export function getDeviceAlarm() {
    return request({
        url: `/index/workbench/count/alarm`,
        method: 'get'
    })
}
// 查询设备数量
export function getDeviceNumber() {
    return request({
        url: `/index/workbench/count/device`,
        method: 'get'
    })
}
// 查询项目数量
export function getProjectNumber() {
    return request({
        url: `/index/workbench/count/project`,
        method: 'get'
    })
}
// 查询工人数量
export function getWorkerNumber() {
    return request({
        url: `/index/workbench/count/worker`,
        method: 'get'
    })
}
// 进行中项目月度考勤统计
export function getAttendMonth(ym) {
    return request({
        url: `/index/workbench/stat/attend-month?ym=${ym}`,
        method: 'get',
    })
}
// 项目进度统计
export function getSchedule() {
    return request({
        url: `/index/workbench/stat/schedule`,
        method: 'get'
    })
}
// 项目月度考核
export function getExamine() {
    return request({
        url: `/index/cockpit/count/project-examine-month`,
        method: 'get'
    })
}

// 分页查询报警
export function alarmPage(data) {
    return request({
        url: '/index/workbench/page-alarm',
        method: 'post',
        data: data
    })
}

// 分页查询待办提醒
export function todoPage(data) {
    return request({
        url: '/index/workbench/page-work',
        method: 'post',
        data: data
    })
}

