import request from '@/utils/request'
// -------------------app信息管理-------------------------
// 分页查询
export function appInfoPageList(data) {
    return request({
        url: '/system/app/info/page',
        method: 'POST',
        data: data
    })
}
// 新增
export function appInfoCreate(data) {
    return request({
        url: '/system/app/info/create',
        method: 'POST',
        data: data
    })
}
// 主键查询详情
export function appInfoGetinfo(id) {
    return request({
        url: `/system/app/info/get?bid=${id}`,
        method: 'GET'
    })
}
// 修改
export function appInfoUpdate(data) {
    return request({
        url: '/system/app/info/update',
        method: 'POST',
        data: data
    })
}
// 批量删除
export function appInfoDelete(ids) {
    return request({
        url: '/system/app/info/delete',
        method: 'POST',
        data: {
            bidList: ids
        }
    })
}
// 停用app
export function appInfoDisable(id) {
    return request({
        url: `/system/app/info/disable/${id}`,
        method: 'GET'
    })
}
// 启用app
export function appInfoEnable(id) {
    return request({
        url: `/system/app/info/enable/${id}`,
        method: 'GET'
    })
}
// -------------------版本管理--------------------
// 分页查询
export function appVersionPageList(data) {
    return request({
        url: '/system/app/version/page',
        method: 'POST',
        data: data
    })
}
// 新增
export function appVersionCreate(data) {
    return request({
        url: '/system/app/version/create',
        method: 'POST',
        data: data
    })
}
// 主键查询详情
export function appVersionGetinfo(id) {
    return request({
        url: `/system/app/version/get?bid=${id}`,
        method: 'GET'
    })
}
// 修改
export function appVersionUpdate(data) {
    return request({
        url: '/system/app/version/update',
        method: 'POST',
        data: data
    })
}
// 批量删除
export function appVersionDelete(ids) {
    return request({
        url: '/system/app/version/delete',
        method: 'POST',
        data: {
            bidList: ids
        }
    })
}
// 停用app
export function appVersionDisable(id) {
    return request({
        url: `/system/app/version/disable/${id}`,
        method: 'GET'
    })
}
// 启用app
export function appVersionEnable(id) {
    return request({
        url: `/system/app/version/enable/${id}`,
        method: 'GET'
    })
}
// 获取APP最新版本
export function appVersionLast(data) {
    return request({
        url: '/system/app/version/last',
        method: 'POST',
        data: data
    })
}