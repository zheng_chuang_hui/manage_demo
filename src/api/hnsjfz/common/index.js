import request from '@/utils/request'

// 上传单个文件
export function uploadSingle(data) {
    return request({
        url: '/system/file/upload/single',
        method: 'post',
        data: data
    })
}
// 获取地区列表
export function getAreaList(data) {
    return request({
        url: `/system/area/list`,
        method: 'POST',
        data
    })
}
// 获取字典数据
export function commonData() {
    return request({
        url: `/comm/data/getAllDict`,
        method: 'GET'
    })
}
// 获取可选下一环节(共用)
export function getNextAct(procInstBid) {
    return request({
        url: `/bpm/procinst/next-acti/${procInstBid}`,
        method: 'GET'
    })
}
// 分页查询审核意见(共用)
export function optionListPage(data) {
    return request({
        url: `/bpm/procinst/page-opinion`,
        method: 'POST',
        data
    })
}

// 获取OSS信息
export function ossInfo(dir) {
    return request({
        url: `file/alioss/info/${dir}`,
        method: 'GET'
    })
}