import request from '@/utils/request'

// 新增
export function deviceCreate(data) {
    return request({
        url: '/device/info/create',
        method: 'post',
        data: data
    })
}

// 批量新增
export function deviceCreateBatch(data) {
    return request({
        url: `/device/info/create-batch`,
        method: 'post',
        data: data
    })
}

// 分页查询
export function devicePage(data) {
    return request({
        url: '/device/info/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function deviceUpdate(data) {
    return request({
        url: '/device/info/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function deviceDelList(ids) {
    return request({
        url: '/device/info/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function deviceDel(id) {
    return request({
        url: `/device/info/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function deviceGet(id) {
    return request({
        url: `/device/info/get/${id}`,
        method: 'get'
    })
}

// 导出设备
export function deviceExport(data) {
    return request({
        url: `/device/info/export`,
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
