import request from '@/utils/request'

// 批量新增项目工人
export function workerSelectCreate(data) {
    return request({
        url: '/project/worker/add-to-project',
        method: 'post',
        data: data
    })
}
// 新增项目工人
export function workerCreate(data) {
    return request({
        url: '/project/worker/create',
        method: 'post',
        data: data
    })
}
// 修改项目工人
export function workerUpdate(data) {
    return request({
        url: '/project/worker/update',
        method: 'post',
        data: data
    })
}

// 分页查询项目工人
export function workerPage(data) {
    return request({
        url: '/project/worker/page-worker',
        method: 'post',
        data: data
    })
}

// 批量移除工人
export function workerDelList(data) {
    return request({
        url: '/project/worker/remove',
        method: 'post',
        data: data
    })
}

// 主键查询设备详情(查看)
export function getDeviceInfo(id) {
    return request({
        url: `/project/worker/getDeviceInfo/${id}`,
        method: 'get'
    })
}

// 工人绑定项目设备
export function workerBindDevice(data) {
    return request({
        url: '/project/worker/bind-device',
        method: 'post',
        data: data
    })
}

// 主键查询工人详情(编辑)
export function getWorkerInfo(workerId) {
    return request({
        url: `/project/worker/getWorkerInfo/${workerId}`,
        method: 'get',
    })
}

// 主键查询工人详情(编辑)
export function getProjectWorkerInfo(workerId) {
    return request({
        url: `/project/worker/getProjectWorkerInfo/${workerId}`,
        method: 'get',
    })
}

// 分页查询项目可选工人
export function workerSelectBind(data) {
    return request({
        url: '/project/worker/page-select-worker',
        method: 'post',
        data: data
    })
}

// 分页查询项目可绑设备
export function workerSelectDevice(data) {
    return request({
        url: '/project/worker/page-select-device',
        method: 'post',
        data: data
    })
}

// 工人解绑项目设备
export function workerUnbindDevice(data) {
    return request({
        url: '/project/worker/unbind-device',
        method: 'post',
        data: data
    })
}