import request from '@/utils/request'

// 新增
export function projectCreate(data) {
    return request({
        url: '/project/info/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function projectPage(data) {
    return request({
        url: '/project/info/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function projectUpdate(data) {
    return request({
        url: '/project/info/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function projectDelList(ids) {
    return request({
        url: '/project/info/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function projectDel(id) {
    return request({
        url: `/project/info/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function projectGet(id) {
    return request({
        url: `/project/info/get/${id}`,
        method: 'get'
    })
}

// 分页查询
export function projectSelectUser(data) {
    return request({
        url: '/project/info/page-select-user',
        method: 'post',
        data: data
    })
}

// 查询我的所有项目
export function projectGetMylist() {
    return request({
        url: `/project/info/list-all`,
        method: 'get'
    })
}

// 查询我的所有项目班组
export function projectGetMyTeamlist(data) {
    return request({
        url: `/project/info/list-all-team`,
        method: 'get',
        params: data
    })
}

// 主键查询我的项目工人详情(公共)
export function getWorkerInfo(id) {
    return request({
        url: `/project/info/getWorkerInfo/${id}`,
        method: 'get'
    })
}

// 查询我的项目工人
export function getAllmyWorker(data) {
    return request({
        url: `/project/info/page-my-worker`,
        method: 'post',
        data
    })
}

// 查询我的项目老务公司(公共)
export function getAllMyCompany() {
    return request({
        url: `/project/info/list-all-company`,
        method: 'get',
    })
}


// 导出每日考勤汇总
export function exportExcel(data) {
    return request({
        url: `/attend/record/excel-sumday`,
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
