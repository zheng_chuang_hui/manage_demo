import request from '@/utils/request'

// 新增
export function fenceCreate(data) {
    return request({
        url: '/project/fence/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function fencePage(data) {
    return request({
        url: '/project/fence/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function fenceUpdate(data) {
    return request({
        url: '/project/fence/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function fenceDelList(ids) {
    return request({
        url: '/project/fence/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function fenceDel(id) {
    return request({
        url: `/project/fence/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function fenceGet(id) {
    return request({
        url: `/project/fence/get/${id}`,
        method: 'get'
    })
}

// 查询项目所有围栏
export function getAllfence(projectId) {
    return request({
        url: `/project/fence/list-all/${projectId}`,
        method: 'get'
    })
}

