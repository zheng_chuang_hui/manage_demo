import request from '@/utils/request'

// 分页查询月度考核
export function examineMonth(data) {
    return request({
        url: '/project/examine/page-month',
        method: 'post',
        data: data
    })
}

// 分页查询周期考核
export function examinePeriod(data) {
    return request({
        url: '/project/examine/page-period',
        method: 'post',
        data: data
    })
}

// 列表查询考核得分
export function examineListScore(data) {
    return request({
        url: '/project/examine/list-score',
        method: 'post',
        data: data
    })
}