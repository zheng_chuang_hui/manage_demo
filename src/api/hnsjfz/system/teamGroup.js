import request from '@/utils/request'

// 新增
export function laborTeamCreate(data) {
    return request({
        url: '/cfg/laborTeam/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function laborTeamPage(data) {
    return request({
        url: '/cfg/laborTeam/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function laborTeamUpdate(data) {
    return request({
        url: '/cfg/laborTeam/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function laborTeamDelList(ids) {
    return request({
        url: '/cfg/laborTeam/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function laborTeamDel(id) {
    return request({
        url: `/cfg/laborTeam/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function laborTeamGet(id) {
    return request({
        url: `/cfg/laborTeam/get/${id}`,
        method: 'get'
    })
}

// 查询所有
export function laborTeamGetAllList(data) {
    return request({
        url: `/cfg/laborTeam/list-all`,
        method: 'get',
        params: data
    })
}
