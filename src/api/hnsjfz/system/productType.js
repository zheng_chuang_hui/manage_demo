import request from '@/utils/request'

// 新增
export function productTypeCreate(data) {
    return request({
        url: '/cfg/productType/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function productTypePage(data) {
    return request({
        url: '/cfg/productType/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function productTypeUpdate(data) {
    return request({
        url: '/cfg/productType/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function productTypeDelList(ids) {
    return request({
        url: '/cfg/productType/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function productTypeDel(id) {
    return request({
        url: `/cfg/productType/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function productTypeGet(id) {
    return request({
        url: `/cfg/productType/get/${id}`,
        method: 'get'
    })
}

// 查询所有
export function productTypeGetAllList() {
    return request({
        url: `/cfg/productType/list-all`,
        method: 'get'
    })
}

// 获取机构列表
export function getDeptList() {
    return request({
        url: `/device/info/list-dept`,
        method: 'get'
    })
}
