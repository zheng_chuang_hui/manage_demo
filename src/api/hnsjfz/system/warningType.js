import request from '@/utils/request'

// 新增
export function alarmTypeCreate(data) {
    return request({
        url: '/cfg/alarmType/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function alarmTypePage(data) {
    return request({
        url: '/cfg/alarmType/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function alarmTypeUpdate(data) {
    return request({
        url: '/cfg/alarmType/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function alarmTypeDelList(ids) {
    return request({
        url: '/cfg/alarmType/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function alarmTypeDel(id) {
    return request({
        url: `/cfg/alarmType/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function alarmTypeGet(id) {
    return request({
        url: `/cfg/alarmType/get/${id}`,
        method: 'get'
    })
}

// 查询所有
export function alarmTypeGetAllList() {
    return request({
        url: `/cfg/alarmType/list-all`,
        method: 'get'
    })
}
