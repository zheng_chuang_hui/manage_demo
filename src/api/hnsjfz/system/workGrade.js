import request from '@/utils/request'

// 新增工种
export function workCreate(data) {
    return request({
        url: '/cfg/workTypeLevel/create-type',
        method: 'post',
        data: data
    })
}

// 新增工种等级
export function levelCreate(data) {
    return request({
        url: '/cfg/workTypeLevel/create-level',
        method: 'post',
        data: data
    })
}

// 分页查询工种
export function workPage(data) {
    return request({
        url: '/cfg/workTypeLevel/page-query',
        method: 'post',
        data: data
    })
}

// 修改工种
export function workUpdate(data) {
    return request({
        url: '/cfg/workTypeLevel/update-type',
        method: 'post',
        data: data
    })
}

// 修改工种等级
export function levelUpdate(data) {
    return request({
        url: '/cfg/workTypeLevel/update-level',
        method: 'post',
        data: data
    })
}

// 批量删除工种
export function workDelList(ids) {
    return request({
        url: '/cfg/workTypeLevel/delete-type',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 批量删除工种等级
export function levelDelList(ids) {
    return request({
        url: '/cfg/workTypeLevel/delete-level',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除工种
export function workDel(id) {
    return request({
        url: `/cfg/workTypeLevel/delete-type/${id}`,
        method: 'get'
    })
}

// 单个删除工种等级
export function levelDel(id) {
    return request({
        url: `/cfg/workTypeLevel/delete-level/${id}`,
        method: 'get'
    })
}

// 主键查询详情工种
export function workGet(id) {
    return request({
        url: `/cfg/workTypeLevel/get-type/${id}`,
        method: 'get'
    })
}

// 主键查询详情工种等级
export function levelGet(id) {
    return request({
        url: `/cfg/workTypeLevel/get-level/${id}`,
        method: 'get'
    })
}

// 查询工种下所有等级
export function workGetAllLevl(id) {
    return request({
        url: `/cfg/workTypeLevel/list-all-level/${id}`,
        method: 'get'
    })
}

// 查询所有工种
export function workGetAllList() {
    return request({
        url: `/cfg/workTypeLevel/list-all-type`,
        method: 'get'
    })
}
