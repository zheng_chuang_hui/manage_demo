import request from '@/utils/request'

// 新增
export function projectGroupCreate(data) {
    return request({
        url: '/cfg/projectGroup/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function projectGroupPage(data) {
    return request({
        url: '/cfg/projectGroup/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function projectGroupUpdate(data) {
    return request({
        url: '/cfg/projectGroup/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function projectGroupDelList(ids) {
    return request({
        url: '/cfg/projectGroup/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function projectGroupDel(id) {
    return request({
        url: `/cfg/projectGroup/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function projectGroupGet(id) {
    return request({
        url: `/cfg/projectGroup/get/${id}`,
        method: 'get'
    })
}

// 查询所有
export function projectGroupGetAllList() {
    return request({
        url: `/cfg/projectGroup/list-all`,
        method: 'get'
    })
}
