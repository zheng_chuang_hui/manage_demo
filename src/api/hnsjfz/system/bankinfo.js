import request from '@/utils/request'

// 新增
export function bankCreate(data) {
    return request({
        url: '/cfg/bank/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function bankPage(data) {
    return request({
        url: '/cfg/bank/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function bankUpdate(data) {
    return request({
        url: '/cfg/bank/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function bankDelList(ids) {
    return request({
        url: '/cfg/bank/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function bankDel(id) {
    return request({
        url: `/cfg/bank/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function bankGet(id) {
    return request({
        url: `/cfg/bank/get/${id}`,
        method: 'get'
    })
}

// 查询所有
export function bankGetAllList() {
    return request({
        url: `/cfg/bank/list-all`,
        method: 'get'
    })
}
