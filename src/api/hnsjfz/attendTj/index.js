import request from '@/utils/request'
import excelRequest from '@/utils/excelRequest'

// ------------------考勤统计--------------------------
// 项目
export function monthStatisProject(data) {
    return request({
        url: '/attend/monthStatis/page-project',
        method: 'post',
        data: data
    })
}

// 班组
export function monthStatisTeam(data) {
    return request({
        url: '/attend/monthStatis/page-team',
        method: 'post',
        data: data
    })
}

// 工人
export function monthStatisWorker(data) {
    return request({
        url: '/attend/monthStatis/page-worker',
        method: 'post',
        data: data
    })
}

// 明细
export function monthStatisDetail(data) {
    return request({
        url: '/attend/monthStatis/page-detail',
        method: 'post',
        data: data
    })
}


// 按项目导出
export function monthStatisExcelProject(data) {
    return request({
        url: '/attend/monthStatis/excel-project',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
// 按班组导出
export function monthStatisExcelTeam(data) {
    return request({
        url: '/attend/monthStatis/excel-team',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
// 按工人导出
export function monthStatisExcelWorker(data) {
    return request({
        url: '/attend/monthStatis/excel-worker',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
// 按明细导出
export function monthStatisExcelDetail(data) {
    return request({
        url: '/attend/monthStatis/excel-detail',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
// -----------------------月度汇总---------------------
// 项目
export function monthSummaryProject(data) {
    return request({
        url: '/attend/monthSummary/page-project',
        method: 'post',
        data: data
    })
}

// 班组
export function monthSummaryTeam(data) {
    return request({
        url: '/attend/monthSummary/page-team',
        method: 'post',
        data: data
    })
}

// 工人
export function monthSummaryWorker(data) {
    return request({
        url: '/attend/monthSummary/page-worker',
        method: 'post',
        data: data
    })
}

// 明细
export function monthSummaryDetail(data) {
    return request({
        url: '/attend/monthSummary/page-detail',
        method: 'post',
        data: data
    })
}

// 按项目导出
export function monthSummaryExcelProject(data) {
    return request({
        url: '/attend/monthSummary/excel-project',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
// 按班组导出
export function monthSummaryExcelTeam(data) {
    return request({
        url: '/attend/monthSummary/excel-team',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
// 按工人导出
export function monthSummaryExcelWorker(data) {
    return request({
        url: '/attend/monthSummary/excel-worker',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
// 按明细导出
export function monthSummaryExcelDetail(data) {
    return request({
        url: '/attend/monthSummary/excel-detail',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}


// -----------------------工资统计---------------------
// 项目
export function monthSalaryProject(data) {
    return request({
        url: '/attend/monthSalary/page-project',
        method: 'post',
        data: data
    })
}

// 班组
export function monthSalaryTeam(data) {
    return request({
        url: '/attend/monthSalary/page-team',
        method: 'post',
        data: data
    })
}

// 工人
export function monthSalaryWorker(data) {
    return request({
        url: '/attend/monthSalary/page-worker',
        method: 'post',
        data: data
    })
}

// 明细
export function monthSalaryDetail(data) {
    return request({
        url: '/attend/monthSalary/page-detail',
        method: 'post',
        data: data
    })
}

// -------------- 详情 -------------------------
// 列表查询考勤记录(共用)
export function attendRecordCommon(data) {
    return request({
        url: '/attend/record/list',
        method: 'post',
        data: data
    })
}

// 列表查询补卡信息(共用)
export function attendCorrectCommon(data) {
    return request({
        url: '/attend/correct/list',
        method: 'post',
        data: data
    })
}

// 列表查询计件信息(共用)
export function attendPieceCommon(data) {
    return request({
        url: '/attend/piece/list',
        method: 'post',
        data: data
    })
}
// --------------------项目统计-----------------------------

// 项目统计
export function projectStatistics(data) {
    return request({
        url: '/project/statistics/stat-list',
        method: 'post',
        data: data
    })
}
// 项目统计导出
export function projectStatisticsExport(data) {
    return excelRequest({
        url: '/project/statistics/stat-list-excel',
        method: 'post',
        data: data
    })
}

// 项目异常统计
export function projectExceptionStatistics(data) {
    return request({
        url: '/project/statistics/stat-exception-list',
        method: 'post',
        data: data
    })
}
// 项目异常统计导出
export function projectExceptionStatisticsExport(data) {
    return excelRequest({
        url: '/project/statistics/stat-exception-excel',
        method: 'post',
        data: data
    })
}