import request from '@/utils/request'

// 新增
export function helpCreate(data) {
    return request({
        url: '/cfg/help/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function helpPage(data) {
    return request({
        url: '/cfg/help/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function helpUpdate(data) {
    return request({
        url: '/cfg/help/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function helpDelList(ids) {
    return request({
        url: '/cfg/help/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 主键查询详情
export function helpGet(id) {
    return request({
        url: `/cfg/help/get/${id}`,
        method: 'get'
    })
}