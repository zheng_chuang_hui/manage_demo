import request from '@/utils/request'

// -----------------------设备--------------------------
// 分页查询
export function alarmDevicePage(data) {
    return request({
        url: '/alarm/device/page-query',
        method: 'post',
        data: data
    })
}

// 批量删除
export function alarmDeviceDelList(ids) {
    return request({
        url: '/alarm/device/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function alarmDeviceDel(id) {
    return request({
        url: `/alarm/device/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function alarmDeviceGet(id) {
    return request({
        url: `/alarm/device/get/${id}`,
        method: 'get'
    })
}
// 处理设备报警
export function alarmDeviceHandle(data) {
    return request({
        url: '/alarm/device/handle',
        method: 'post',
        data: data
    })
}
// 设备报警 导出
export function alarmDeviceExport(data) {
    return request({
        url: '/alarm/device/export',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}

// ------------------天气----------------------
// 分页查询
export function alarmWeatherPage(data) {
    return request({
        url: '/alarm/weather/page-query',
        method: 'post',
        data: data
    })
}
// -------------------进度--------------------
// 分页查询
export function alarmProjectPage(data) {
    return request({
        url: '/alarm/project/page-query',
        method: 'post',
        data: data
    })
}
// 设备进度报警 导出
export function alarmProjectExport(data) {
    return request({
        url: '/alarm/project/export',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}
// ------------------- 设备报警 --------------
// 查询
export function alarmStatList(data) {
    return request({
        url: '/alarm-stat/device/list',
        method: 'post',
        data: data
    })
}
// 导出
export function alarmStatExport(data) {
    return request({
        url: '/alarm-stat/device/export',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}