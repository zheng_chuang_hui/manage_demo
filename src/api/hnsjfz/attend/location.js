import request from '@/utils/request'

// 分页查询工人信息
export function locationWorkers(data) {
    return request({
        url: '/attend/location/page-worker',
        method: 'post',
        data: data
    })
}