import request from '@/utils/request'

// 新增
export function attendCorrectCreate(data) {
    return request({
        url: '/attend/correct/create-todo',
        method: 'post',
        data: data
    })
}

// 分页查询
export function attendCorrectPage(data) {
    return request({
        url: '/attend/correct/page-todo',
        method: 'post',
        data: data
    })
}


// 修改
export function attendCorrectUpdate(data) {
    return request({
        url: '/attend/correct/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function attendCorrectDelList(ids) {
    return request({
        url: '/attend/correct/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function attendCorrectDel(id) {
    return request({
        url: `/attend/correct/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function attendCorrectGet(id) {
    return request({
        url: `/attend/correct/get/${id}`,
        method: 'get'
    })
}



// 分页查询可选工人
export function attendWorkerPage(data) {
    return request({
        url: '/attend/correct/page-worker',
        method: 'post',
        data: data
    })
}

// 主键查询详情
export function attendWorkerInfo(id) {
    return request({
        url: `/attend/correct/getWorkerInfo/${id}`,
        method: 'get'
    })
}