import request from '@/utils/request'

// 分页查询
export function monthBpmPage(data) {
    return request({
        url: '/attend/monthBpm/page-query',
        method: 'post',
        data: data
    })
}
// 流程提交
export function monthBpmSubmit(data) {
    return request({
        url: '/attend/monthBpm/proc-submit',
        method: 'post',
        data: data
    })
}