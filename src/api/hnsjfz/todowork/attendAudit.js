import request from '@/utils/request'


// 分页查询
export function correctBpmPage(data) {
    return request({
        url: 'attend/correctBpm/page-todo',
        method: 'post',
        data: data
    })
}

// 提交
export function correctBpmSubmit(data) {
    return request({
        url: '/attend/correctBpm/proc-submit',
        method: 'post',
        data: data
    })
}
// 主键查询考勤补卡详情
export function correctBpmGetInfo(id) {
    return request({
        url: `/attend/correctBpm/get/${id}`,
        method: 'get',
    })
}
// 