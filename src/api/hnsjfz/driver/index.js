import request from '@/utils/request'

// 设备报警
export function deviceAlarm(data) {
    return request({
        url: '/index/cockpit/alarm/page',
        method: 'post',
        data: data
    })
}
// 项目工人统计
export function projectWorker() {
    return request({
        url: '/index/cockpit/count/project-worker',
        method: 'get',
    })
}
// 用工分析
export function WorkerTypeWorker() {
    return request({
        url: '/index/cockpit/count/worktype-worker',
        method: 'get',
    })
}
// 获取定位安全帽列表
export function listLocation() {
    return request({
        url: '/index/cockpit/device/list-location',
        method: 'get',
    })
}
// 获取视频安全帽列表
export function listVideo() {
    return request({
        url: '/index/cockpit/device/list-video',
        method: 'get',
    })
}
// 获取项目列表
export function projectList() {
    return request({
        url: '/index/cockpit/project/list',
        method: 'get',
    })
}
// 获取项目进度
export function scheduleList() {
    return request({
        url: '/index/cockpit/schedule/list',
        method: 'get',
    })
}
// 项目进度图片
export function scheduleListPic() {
    return request({
        url: '/index/cockpit/schedule/list-pic',
        method: 'get',
    })
}
// 项目进度视频
export function scheduleListVideo() {
    return request({
        url: '/index/cockpit/schedule/list-video',
        method: 'get',
    })
}
// 考勤统计
export function attendSum() {
    return request({
        url: '/index/cockpit/sum/attend',
        method: 'get',
    })
}
// 主键查询设备详情
export function deviceInfo(id) {
    return request({
        url: `index/cockpit/device/get/${id}`,
        method: 'get',
    })
}
// 主键查询项目详情
export function projectInfo(id) {
    return request({
        url: `index/cockpit/project/get/${id}`,
        method: 'get',
    })
}
// 主键查询项目详情
export function newProjectInfo(id) {
    return request({
        url: `index/cockpit/project/get-detail/${id}`,
        method: 'get',
    })
}
export function getCityWeather() {
    return request({
        url: `https://restapi.amap.com/v3/weather/weatherInfo?city=郑州&key=14dd98d20a3611856eca5397c2be6311&extensions=all`,
        // url: `https://v0.yiketianqi.com/api?unescape=1&version=v91&appid=43656176&appsecret=I42og6Lm&ext=&cityid=&city=郑州`,
        method: 'get',
    })
}
// 设备预警
export function getAlarm() {
    return request({
        url: `/index/cockpit/count/project-alarm`,
        method: 'get'
    })
}

export function getProjectWeather(data) {
    return request({
        url: `web/comm/weather/lives`,
        method: 'post',
        data: data
    })
}