import request from '@/utils/request'

// 新增
export function workerContractCreate(data) {
    return request({
        url: '/worker/contract/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function workerContractPage(data) {
    return request({
        url: '/worker/contract/page-query',
        method: 'post',
        data: data
    })
}


// 修改
export function workerContractUpdate(data) {
    return request({
        url: '/worker/contract/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function workerContractDelList(ids) {
    return request({
        url: '/worker/contract/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function workerContractDel(id) {
    return request({
        url: `/worker/contract/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function workerContractGet(id) {
    return request({
        url: `/worker/contract/get/${id}`,
        method: 'get'
    })
}

// 列表查询合同(共用)
export function workerContractCommonGet(id) {
    return request({
        url: `/worker/contract/list/${id}`,
        method: 'get'
    })
}
