import request from '@/utils/request'

// 新增
export function workerInfoCreate(data) {
    return request({
        url: '/worker/info/create',
        method: 'post',
        data: data
    })
}

// 分页查询
export function workerInfoPage(data) {
    return request({
        url: '/worker/info/page-query',
        method: 'post',
        data: data
    })
}

// 培训-选择工人
export function trainPageWorker(data) {
    return request({
        url: '/worker/train/page-worker',
        method: 'post',
        data: data
    })
}


// 修改
export function workerInfoUpdate(data) {
    return request({
        url: '/worker/info/update',
        method: 'post',
        data: data
    })
}

// 批量删除
export function workerInfoDelList(ids) {
    return request({
        url: '/worker/info/delete',
        method: 'post',
        data: {
            idList: ids
        }
    })
}

// 单个删除
export function workerInfoDel(id) {
    return request({
        url: `/worker/info/delete/${id}`,
        method: 'get'
    })
}

// 主键查询详情
export function workerInfoGet(id) {
    return request({
        url: `/worker/info/get/${id}`,
        method: 'get'
    })
}

// 身份证号查询详情
export function workerInfoGetAllList(idcardNo) {
    return request({
        url: `/worker/info/getByIdcard/${idcardNo}`,
        method: 'get'
    })
}

// 导出
export function workerInfoExport(data) {
    return request({
        url: '/worker/info/export',
        method: 'get',
        params: data,
        responseType: 'blob'
    })
}