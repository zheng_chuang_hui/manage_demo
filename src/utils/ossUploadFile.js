import axios from 'axios'


// 阿里oss 文件上传
/* 
    key   // 存储在oss的文件路径  flyPic+filename+/+file.name
    OSSAccessKeyId     // accessKeyId
    policy    // accessKeyId
    Signature  // 签名
    file  // 文件
    success_action_status:200  // 成功状态码
*/

export function aliOssFile(data, url) {
    return axios({
        url: url,//这里的地址就是oss的地址
        method: 'POST',
        data: data,
        // withCredentials: false,
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
    // return request({
    //     headers: {
    //         'Content-Type': data.type
    //     },
    //     url: url,
    //     method: 'post',
    //     data
    // })
}