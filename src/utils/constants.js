/**
 * 枚举类
 */

/**
 * 全局通用状态枚举
 */
export const SysCommonStatusEnum = {
  ENABLE: 1, // 开启
  DISABLE: 0 // 禁用
}

/**
 * 菜单的类型枚举
 */
export const SysMenuTypeEnum = {
  DIR: 1, // 目录
  MENU: 2, // 菜单
  BUTTON: 3 // 按钮
}

/**
 * 角色的类型枚举
 */
export const SysRoleTypeEnum = {
  SYSTEM: 1, // 内置角色
  CUSTOM: 2 // 自定义角色
}

/**
 * 数据权限的范围枚举
 */
export const SysDataScopeEnum = {
  ALL: 1, // 全部数据权限
  DEPT_CUSTOM: 2, // 指定部门数据权限
  DEPT_ONLY: 3, // 部门数据权限
  DEPT_AND_CHILD: 4, // 部门及以下数据权限
  DEPT_SELF: 5 // 仅本人数据权限
}

/**
 * 代码生成模板类型
 */
export const ToolCodegenTemplateTypeEnum = {
  CRUD: 1, // 基础 CRUD
  TREE: 2, // 树形 CRUD
  SUB: 3, // 主子表 CRUD
}

/**
 * 任务状态的枚举
 */
export const InfJobStatusEnum = {
  INIT: 0, // 初始化中
  NORMAL: 1, // 运行中
  STOP: 2, // 暂停运行
}

/**
 * API 异常数据的处理状态
 */
export const InfApiErrorLogProcessStatusEnum = {
  INIT: 0, // 未处理
  DONE: 1, // 已处理
  IGNORE: 2, // 已忽略
}

/**
 *  文化程度
 */
export const education = [
  { name: '小学', value: '小学' },
  { name: '初中', value: '初中' },
  { name: '高中', value: '高中' },
  { name: '专科', value: '专科' },
  { name: '本科', value: '本科' },
  { name: '研究生', value: '研究生' },
]

/**
 *  试用期
 */
export const probation = [
  { name: '无', value: '' },
  { name: '1月', value: '1月' },
  { name: '2月', value: '2月' },
  { name: '3月', value: '3月' },
  { name: '4月', value: '4月' },
  { name: '5月', value: '5月' },
  { name: '6月', value: '6月' },
]

/**
 *  OSS文件的目录
 */
export const fileCatalog = [
  { name: '项目附件', value: 'project' },
  { name: '项目进度', value: 'schedule' },
  { name: '合同附件', value: 'contract' },
  { name: '培训附件', value: 'train' },
]


