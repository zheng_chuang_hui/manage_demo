import JsSIP from 'jssip'
import { Notification, MessageBox, Message } from 'element-ui'

export const jssipInit = (boolean) => {
    /*
      boolean用于开启关闭jssip自带的打印
    */
    let Session = null;
    let userAgent = null
    let outgoingSession = null; //用于分辨呼出
    let incomingSession = null; //用于分辨呼入
    let localStream = new MediaStream(); //媒体流
    let audioView = document.getElementById('videoView'); //audio标签

    console.log('audioView', audioView);

    JsSIP.C.SESSION_EXPIRES = 120
    JsSIP.C.MIN_SESSION_EXPIRES = 120

    if (boolean) JsSIP.debug.enable('JsSIP:*');
    else JsSIP.debug.disable('JsSIP:*');

    // 分机号注册
    function reg(sip_info, registerCallbacks) {
        let sip_id = sip_info.sip_id,
            sip_pwd = sip_info.sip_pwd,
            sip_host = sip_info.sip_host,
            wss_url = sip_info.wss_url,
            stun_host = sip_info.stun_host,
            turn_host = sip_info.turn_host,
            turn_pwd = sip_info.turn_pwd,
            turn_user = sip_info.turn_user;
        let config = {
            sockets: [new JsSIP.WebSocketInterface(wss_url)],
            uri: sip_id + '@' + sip_host,//此sip_id为拨打者账号
            transportOptions: {
                wsServers: [wss_url],
                connectionTimeout: 30
            },
            authorizationUser: sip_id,
            password: sip_pwd,
            sessionDescriptionHandlerFactoryOptions: {
                peerConnectionOptions: {
                    rtcConfiguration: {
                        iceServers: [
                            { urls: 'stun:' + stun_host },
                            {
                                urls: 'turn:' + turn_host,
                                username: turn_user,
                                credential: turn_pwd
                            }
                        ]
                    }
                }
            }
        }
        //创建user agent
        userAgent = new JsSIP.UA(config);
        userAgent.start()
        const { registrationFailed, registered, registrationExpiring } = registerCallbacks
        /*
         确保每次流程只注册一次，保证下次注册之前注销
         不然会建立多个ws链接，导致服务器找不到正确的接口
         和多次重连与连接不稳定
        */

        //注册成功
        userAgent.on('registered', registered)

        //注册即将失效
        userAgent.on('registrationExpiring', registrationExpiring)

        //注册失败
        userAgent.on('registrationFailed', registrationFailed)

        //呼入与呼出
        userAgent.on('newRTCSession', function (data) {
            Session = data.session
            if (data.originator == "local") {
                //外呼
                outgoingSession = data.session
                outgoingSession.connection.addEventListener('track', (event) => {
                    console.log('event', event);
                    audioView.srcObject = event.streams[0];
                    audioView.play();
                    audioView.volume = 1;
                });
            } else {
                //呼入
                incomingSession = data.session
                let receivers = incomingSession.connection.getReceivers();
                incomingSession.on('accepted', function (data) {
                    if (receivers) {
                        receivers.forEach(item => {
                            localStream.addTrack(item.track);
                        })
                        audioView.srcObject = localStream;
                        audioView.play();
                        audioView.volume = 1;
                    }
                })

                incomingSession.on("progress", function (data) {
                    console.log('来电振铃')
                })

                incomingSession.on('failed', function (e) {
                    console.log('来电通话失败')
                })

                incomingSession.on('ended', function (e) {
                    console.log('来电挂断')
                });

            }

            // 静音时触发
            Session.on('muted', function (data) {
                console.log('通话静音中')
            })

            //取消静音时触发
            Session.on('unmuted', function (data) {
                console.log('恢复通话')
            })

            //发送DTMF讯号的回调
            Session.on('newDTMF', function (data) {
                let dtmfSession = data.dtmf
                dtmfSession.on('succeeded', function (data) {
                    console.log('发送成功')
                })

                dtmfSession.on('failed', function (data) {
                    console.log('发送失败')
                })


            })


        })
    }

    //注销注册
    function unreg() {
        let options = {
            all: true
        }
        userAgent.unregister(options)
        userAgent = null
    }

    // 拨号
    function testCall(phoneNum, isVideo) {
        var eventHandlers = {
            //振铃中
            progress: function () {
                console.log('振铃中')
                Message({ message: '振铃中', type: 'warning' })
            },
            //呼叫失败
            failed: function (e) {
                console.log('e', e);
                Message({ message: '呼叫失败', type: 'error' })
                console.log('呼叫失败')
            },
            //通话结束
            ended: function () {
                Message({ message: '通话结束', type: 'warning' })
                console.log('通话结束')
            },
            //通话中
            confirmed: function () {
                Message({ message: '通话中', type: 'success' })
                console.log('通话中')
            }
        }

        var options = {
            'eventHandlers': eventHandlers,
            'extraHeaders': ['X-Foo: foo', 'X-Bar: bar'],
            'mediaConstraints': {
                'audio': true,
                'video': isVideo,
                mandatory: {
                    maxWidth: 640,
                    maxHeight: 360
                },
            },
        };
        setTimeout(() => {
            userAgent.call(String(phoneNum), options)
        }, 100);
    }

    // 挂断
    function hangup() {
        userAgent.terminateSessions()
    }


    //静音的方法
    function Mute() {
        let options = {
            audio: true,
            video: true,
        }
        Session.mute(options)
    }

    //取消静音的方法
    function unMute() {
        let options = {
            audio: true,
            video: true
        }
        Session.unmute(options)
    }

    //保持的方法\
    function Hold() {
        let options = {
            useUpdate: false
        }
        let done = function () {
            console.log('保持中')
        }
        Session.hold(options, done)
    }

    //取消保持的方法
    function unHold() {
        let options = {
            useUpdate: false
        }
        let done = function () {
            console.log('取消保持')
        }
        Session.unhold(options, done)
    }

    //输出DTMF信号
    function sendtmf(value) {
        Session.sendDTMF(value)
    }


    //接听
    function answer() {
        let options = {
            'extraHeaders': ['X-Foo: foo', 'X-Bar: bar'],
            'mediaConstraints': {
                'audio': true,
                'video': false,
            },
            // 'mediaStream': localStream,
        }
        Session.answer(options);

    }

    return {
        reg,
        unreg,
        testCall,
        hangup,
        answer,
        Mute,
        unMute,
        Hold,
        unHold,
        sendtmf
    }

}
